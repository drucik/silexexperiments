<?php
namespace CrazyStudio;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class SomeControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->post('some', function (Request $request) use ($app) {
            $post = array(
                'title' => $request->request->get('title'),
                'body' => $request->request->get('body'),
            );
            //$post['id'] = createPost($post);

            return $app->json($post, 201);
        });
        return $controllers;
    }
}