<?php
namespace CrazyStudio;

use Silex\Application;
use Silex\ControllerProviderInterface;

class BlogControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        $blogPosts = array(
            1 => array(
                'date'      => '2011-03-29',
                'author'    => 'igorw',
                'title'     => 'Using Silex',
                'body'      => '...'
            ),
        );

        $controllers->get('blog', function () use ($blogPosts) {
            $output = '';
            foreach ($blogPosts as $post) {
                $output .= $post['title'];
                $output .= '<br />';
            }

            return $output;
        });

        $controllers->get('blog/{id}', function (Application $app, $id) use ($blogPosts) {
            if (!isset($blogPosts[$id])) {
                $app->abort(404, "Post {$id} does not exists");
            }
            $post = $blogPosts[$id];

            return "<h1>{$post['title']}</h1>".
            "<p>{$post['body']}</p>";
        })
            ->assert('id', '\d+')
            ->convert('id', function ($id) { return (int) $id; });

        $controllers->put('blog/{id}', function ($id) {

        });

        $controllers->delete('blog/{$id}', function ($id) {

        });

        $controllers->get('blog/{$postId}/{$commentId}', function ($postId, $commentId) {

        });
        return $controllers;
    }
}