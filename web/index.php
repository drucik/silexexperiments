<?php
use CrazyStudio\Models\Event;
use CrazyStudio\Providers\UserProvider;
use Monolog\Logger;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Silex\Provider\ValidatorServiceProvider;

$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('CrazyStudio', __DIR__);

$app = new Silex\Application();
$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../logs/app.log',
    'monolog.level'   => Logger::DEBUG
));

$app->register(new ValidatorServiceProvider());
$app->register(new SessionServiceProvider());
$app->register(new SecurityServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new TranslationServiceProvider(), array(
    'locale' => 'pl'
));
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views'
));
$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host'   => 'localhost',
        'user'   => 'root',
        'dbname' => 'sample_silex'
    )
));

require_once __DIR__.'/CrazyStudio/Migrations/UsersMigration.php';

$app['security.firewalls'] = array(
    'login' => array(
        'pattern' => '^/login$'
    ),
    'admin' => array(
        'pattern' => '^.*$',
        'form'    => array('login_path' => '/login', 'check_path' => '/login_check'),
        'logout'  => array('logout_path' => '/admin/logout'),
        'users'   => $app->share(function () use ($app) {
                return new UserProvider($app['db']);
            }),
    ),
);

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->get('/login', function (Request $request) use ($app)
{
    return $app['twig']->render('login.twig', array(
        'error'         => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),
    ));
});

$app->post('/feedback', function (Request $request) {
    $message = $request->get('message');
    mail('feedback@yoursite.com', '[YourSite] Feedback', $message);

    return new Response('Thank you for your feedback!', 201);
});

$app->get('/admin', function (Request $request) use ($app) {
    $token = $app['security']->getToken();
    if (null !== $token)
        $user = $token->getUser();
    return $app['twig']->render('admin.twig');
});

$app->get('/validate/{title}', function ($title) use ($app) {
    $event = new Event();
    $event->title = $title;

    $errors = $app['validator']->validate($event);
    $jsonErrors = array();
    if (count($errors) > 0)
    {
        foreach ($errors as $error) {
            $jsonErrors[$error->getPropertyPath()] = $error->getMessage();
        }
    } else {
        echo 'The author is valid';
    }

    return $app->json(array('errors' => $jsonErrors));
});

Request::enableHttpMethodParameterOverride();

$app->mount('/api', new \CrazyStudio\BlogControllerProvider());
$app->mount('/api', new \CrazyStudio\SomeControllerProvider());
$app->run();
